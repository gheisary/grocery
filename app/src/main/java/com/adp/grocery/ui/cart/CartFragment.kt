package com.adp.grocery.ui.cart

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import butterknife.OnClick
import com.adp.grocery.R
import com.adp.grocery.core.domain.cart.Cart
import com.adp.grocery.core.domain.cart.CartProduct
import com.adp.grocery.core.domain.cart.CartProductFactory
import com.adp.grocery.core.domain.cart.CartService
import com.adp.grocery.inject.Injector
import com.adp.grocery.ui.base.BaseFragment
import com.adp.grocery.ui.util.PriceFormatter
import com.adp.grocery.util.RxUtil
import io.reactivex.functions.Consumer

class CartFragment : BaseFragment(), CartListener {

    @BindView(R.id.cart_emptyView)
    internal var emptyView: View? = null

    @BindView(R.id.cart_progressbar)
    internal var progressBar: ProgressBar? = null

    @BindView(R.id.cart_recyclerview)
    internal var recyclerView: RecyclerView? = null

    @BindView(R.id.cart_content)
    internal var contentView: View? = null

    @BindView(R.id.cart_checkout_total_textview)
    internal var checkoutTotal: TextView? = null

    private var cartService: CartService? = null

    override fun layoutId(): Int {
        return R.layout.cart_fragment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        cartService = Injector.cartService()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView!!.setHasFixedSize(true)
        recyclerView!!.layoutManager = LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)
    }

    override fun onStart() {
        super.onStart()

        showProgressBar()

        addDisposable(cartService!!.cart
            .compose(RxUtil.applyObservableSchedulers())
            .subscribe(Consumer<Cart> { this.onCartResponse(it) }, RxUtil.emptyConsumer())
        )
    }

    private fun showProgressBar() {
        progressBar!!.visibility = View.VISIBLE
        emptyView!!.visibility = View.GONE
        contentView!!.visibility = View.GONE
    }

    private fun onCartResponse(cart: Cart) {
        hideProgressBar()
        if (cart.products!!.isEmpty()) {
            showEmptyCart()
        } else {
            showContentCart(cart)
        }
    }

    private fun hideProgressBar() {
        progressBar!!.visibility = View.GONE
    }

    private fun showContentCart(cart: Cart) {
        emptyView!!.visibility = View.GONE
        contentView!!.visibility = View.VISIBLE

        val adapter = CartAdapter(this)
        adapter.setProductList(cart.products!!)
        recyclerView!!.adapter = adapter

        var totalPrice = 0.0
        for (product in cart.products) {
            totalPrice += product.price
        }

        checkoutTotal!!.text = PriceFormatter.format(totalPrice)
    }

    private fun showEmptyCart() {
        emptyView!!.visibility = View.VISIBLE
        contentView!!.visibility = View.GONE
    }

    override fun onCartProductClicked(product: CartProduct) {
        Toast.makeText(context, "Removing... " + product.title!!, Toast.LENGTH_SHORT).show()
        cartService!!.removeProduct(CartProductFactory.newCartProduct(product, 1))
            .compose(RxUtil.applySingleSchedulers())
            .subscribe(RxUtil.emptySingleObserver())
    }

    @OnClick(R.id.cart_checkout_button)
    internal fun onCheckoutClicked() {
        Toast.makeText(context, "Checkout...", Toast.LENGTH_SHORT).show()
        cartService!!.clear()
            .compose(RxUtil.applyCompletableSchedulers())
            .subscribe(RxUtil.emptyCompletableObserver())
    }

    companion object {

        fun newInstance(): CartFragment {
            val fragment = CartFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}