package com.adp.grocery.ui.catalog;

import com.adp.grocery.core.domain.product.Product;

interface CatalogListener {
    void onProductClicked(Product product);
}
