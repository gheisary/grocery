package com.adp.grocery.ui.cart

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.adp.grocery.R
import com.adp.grocery.core.domain.cart.CartProduct

import java.util.ArrayList

internal class CartAdapter(private val listListener: CartListener) : RecyclerView.Adapter<CartItemViewHolder>() {

    private val productList: MutableList<CartProduct>

    init {
        this.productList = ArrayList(20)
    }

    fun setProductList(productList: List<CartProduct>) {
        this.productList.clear()
        this.productList.addAll(productList)
        this.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cart_list_item, parent, false)
        return CartItemViewHolder(view, listListener)
    }

    override fun onBindViewHolder(holder: CartItemViewHolder, position: Int) {
        val product = productList[position]
        holder.bind(product)
    }

    override fun getItemCount(): Int {
        return productList.size
    }


}
