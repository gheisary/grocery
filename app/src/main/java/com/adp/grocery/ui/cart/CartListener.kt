package com.adp.grocery.ui.cart


import com.adp.grocery.core.domain.cart.CartProduct

internal interface CartListener {
    fun onCartProductClicked(product: CartProduct)
}
