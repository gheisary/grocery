package com.adp.grocery.ui

import android.app.Application
import com.adp.grocery.inject.DefaultDependenciesFactory
import com.adp.grocery.inject.InMemoryDataDependenciesFactory
import com.adp.grocery.inject.Injector

class GroceryStoreApp : Application() {

    override fun onCreate() {
        super.onCreate()
        Injector.using(DefaultDependenciesFactory(InMemoryDataDependenciesFactory()))
    }
}
