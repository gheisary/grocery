package com.adp.grocery.ui.base

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity
import butterknife.ButterKnife
import butterknife.Unbinder
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseActivity : AppCompatActivity() {

    private val disposables = CompositeDisposable()

    private var unbinder: Unbinder? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId())
        unbinder = ButterKnife.bind(this)
    }

    @LayoutRes
    protected abstract fun layoutId(): Int

    public override fun onStop() {
        super.onStop()
        disposables.clear()
    }

    override fun onDestroy() {
        unbinder!!.unbind()
        super.onDestroy()
    }

    protected fun addDisposable(disposable: Disposable) {
        disposables.add(disposable)
    }

}
