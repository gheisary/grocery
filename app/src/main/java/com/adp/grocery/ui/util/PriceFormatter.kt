package com.adp.grocery.ui.util

import java.text.NumberFormat
import java.util.Locale

object PriceFormatter {

    fun format(price: Double): String {
        val formattedPrice = NumberFormat.getNumberInstance(Locale.getDefault()).format(price)
        return String.format("%s €", formattedPrice)
    }
}
