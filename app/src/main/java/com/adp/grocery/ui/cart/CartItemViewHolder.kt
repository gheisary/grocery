package com.adp.grocery.ui.cart

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.adp.grocery.R
import com.adp.grocery.core.domain.cart.CartProduct
import com.adp.grocery.ui.util.PriceFormatter
import com.squareup.picasso.Picasso

internal class CartItemViewHolder(view: View, private val listListener: CartListener) : RecyclerView.ViewHolder(view) {

    @BindView(R.id.cart_item_imageview)
    var image: ImageView? = null

    @BindView(R.id.cart_item_name)
    var name: TextView? = null

    @BindView(R.id.cart_item_price)
    var price: TextView? = null

    @BindView(R.id.cart_item_quantity)
    var quantity: TextView? = null

    init {
        ButterKnife.bind(this, view)
    }

    fun bind(product: CartProduct) {
        name!!.text = product.title
        price!!.text = PriceFormatter.format(product.price)
        itemView.setOnClickListener { v -> listListener.onCartProductClicked(product) }

        val quantityFormatted =
            String.format(itemView.resources.getString(R.string.cart_item_quantity), product.quantity)
        quantity!!.text = quantityFormatted

        Picasso.with(itemView.context)
            .load(product.imageUrl)
            .centerInside()
            .fit()
            .into(image)
    }
}
