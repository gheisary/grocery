package com.adp.grocery.ui.catalog;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.adp.grocery.R;
import com.adp.grocery.ui.util.PriceFormatter;
import com.squareup.picasso.Picasso;
import com.adp.grocery.core.domain.product.Product;

class CatalogItemViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.catalog_item_imageview)
    ImageView image;

    @BindView(R.id.catalog_item_name)
    TextView name;

    @BindView(R.id.catalog_item_price)
    TextView price;

    private final CatalogListener listListener;

    CatalogItemViewHolder(View view, CatalogListener listListener) {
        super(view);
        this.listListener = listListener;
        ButterKnife.bind(this, view);
    }

    public void bind(Product product) {
        name.setText(product.getTitle());
        price.setText(PriceFormatter.INSTANCE.format(product.getPrice()));
        itemView.setOnClickListener(v -> listListener.onProductClicked(product));

        Picasso.with(itemView.getContext())
                .load(product.getImageUrl())
                .centerInside()
                .fit()
                .into(image);
    }
}
