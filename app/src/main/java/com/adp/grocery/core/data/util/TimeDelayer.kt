package com.adp.grocery.core.data.util

import java.util.Random

class TimeDelayer {
    private val random: Random

    init {
        random = Random(System.nanoTime())
    }

    @JvmOverloads
    fun delay(maxTime: Int = MAX_DEFAULT_DELAY) {
        try {
            Thread.sleep(random.nextInt(maxTime).toLong())
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }

    companion object {
        private val MAX_DEFAULT_DELAY = 2000 // 2 sec
    }
}
