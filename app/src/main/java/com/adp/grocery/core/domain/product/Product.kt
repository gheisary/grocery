package com.adp.grocery.core.domain.product

open class Product(val sku: String?, val title: String?, val imageUrl: String?, val price: Double) {

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false

        val product = o as Product?

        if (java.lang.Double.compare(product!!.price, price) != 0) return false
        if (if (sku != null) sku != product.sku else product.sku != null) return false
        if (if (title != null) title != product.title else product.title != null) return false
        return if (imageUrl != null) imageUrl == product.imageUrl else product.imageUrl == null
    }

    override fun hashCode(): Int {
        var result: Int
        val temp: Long
        result = sku?.hashCode() ?: 0
        result = 31 * result + (title?.hashCode() ?: 0)
        result = 31 * result + (imageUrl?.hashCode() ?: 0)
        temp = java.lang.Double.doubleToLongBits(price)
        result = 31 * result + (temp xor temp.ushr(32)).toInt()
        return result
    }

    override fun toString(): String {
        val sb = StringBuilder("Product{")
        sb.append("sku='").append(sku).append('\'')
        sb.append(", title='").append(title).append('\'')
        sb.append(", imageUrl='").append(imageUrl).append('\'')
        sb.append(", price=").append(price)
        sb.append('}')
        return sb.toString()
    }
}
