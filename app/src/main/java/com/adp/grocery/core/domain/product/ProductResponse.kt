package com.adp.grocery.core.domain.product

class ProductResponse(val products: List<Product>?) {

    val isEmpty: Boolean
        get() = products == null || products.isEmpty()
}
