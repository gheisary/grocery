package com.adp.grocery.core.domain.cart

import com.adp.grocery.core.domain.product.Product

object CartProductFactory {

    fun newCartProduct(product: Product, quantity: Int): CartProduct {
        if (quantity <= 0) {
            throw IllegalArgumentException("Product should have a positive quantity")
        }

        return CartProduct(product.sku, product.title, product.imageUrl, product.price, quantity)
    }
}
