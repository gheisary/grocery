package com.adp.grocery.core.domain.cart

import io.reactivex.Single

interface CartDataSource {

    val cart: Single<Cart>

    fun addProduct(cartProduct: CartProduct): Single<Cart>

    fun removeProduct(cartProduct: CartProduct): Single<Cart>

    fun emptyCart(): Single<Cart>
}
