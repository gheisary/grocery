package com.adp.grocery.core.data.sources

import com.adp.grocery.core.data.util.TimeDelayer
import com.adp.grocery.core.domain.cart.Cart
import com.adp.grocery.core.domain.cart.CartBuilder
import com.adp.grocery.core.domain.cart.CartDataSource
import com.adp.grocery.core.domain.cart.CartProduct
import io.reactivex.Single

class InMemoryCartDataSource(private val timeDelayer: TimeDelayer) : CartDataSource {
    private var cart: Cart? = null

    init {
        this.cart = Cart.EMPTY
    }

    @Synchronized
    override fun getCart(): Single<Cart> {
        return Single.fromCallable {
            timeDelayer.delay()
            cart
        }
    }

    @Synchronized
    override fun addProduct(cartProduct: CartProduct): Single<Cart> {
        return Single.fromCallable {
            timeDelayer.delay()

            cart = CartBuilder.from(cart!!)
                .addProduct(cartProduct)
                .build()

            cart
        }
    }

    @Synchronized
    override fun removeProduct(cartProduct: CartProduct): Single<Cart> {
        return Single.fromCallable {
            timeDelayer.delay()

            cart = CartBuilder.from(cart!!)
                .removeProduct(cartProduct)
                .build()

            cart
        }
    }

    @Synchronized
    override fun emptyCart(): Single<Cart> {
        return Single.fromCallable {
            timeDelayer.delay()
            cart = Cart.EMPTY
            cart
        }
    }
}
