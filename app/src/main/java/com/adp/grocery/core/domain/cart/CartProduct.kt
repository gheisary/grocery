package com.adp.grocery.core.domain.cart


import com.adp.grocery.core.domain.product.Product

class CartProduct(sku: String, title: String, imageUrl: String, price: Double, val quantity: Int) :
    Product(sku, title, imageUrl, price) {

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        if (!super.equals(o)) return false

        val product = o as CartProduct?

        return quantity == product!!.quantity

    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + quantity
        return result
    }

    override fun toString(): String {
        val sb = StringBuilder("CartProduct{")
        sb.append("quantity=").append(quantity)
        sb.append('}')
        return sb.toString()
    }
}
