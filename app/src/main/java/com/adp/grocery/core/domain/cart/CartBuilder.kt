package com.adp.grocery.core.domain.cart

import java.util.ArrayList

class CartBuilder private constructor(private val products: MutableList<CartProduct>) {

    fun addProduct(product: CartProduct): CartBuilder {
        val index = getProductIndexBySku(product.sku)
        if (index != NOT_FOUND) {
            val includedProduct = products[index]
            val totalQuantity = product.quantity + includedProduct.quantity
            products[index] = CartProduct(product.sku, product.title, product.imageUrl, product.price, totalQuantity)
        } else {
            products.add(product)
        }

        return this
    }

    fun removeProduct(product: CartProduct): CartBuilder {
        val index = getProductIndexBySku(product.sku)
        if (index != NOT_FOUND) {
            val includedProduct = products[index]
            val newQuantity = includedProduct.quantity - product.quantity
            if (newQuantity <= 0) {
                products.removeAt(index)
            } else {
                products[index] = CartProduct(product.sku, product.title, product.imageUrl, product.price, newQuantity)
            }
        }

        return this
    }

    private fun getProductIndexBySku(sku: String): Int {
        for (i in products.indices) {
            if (sku.equals(products[i].sku, ignoreCase = true)) {
                return i
            }
        }

        return NOT_FOUND
    }

    fun build(): Cart {
        return Cart(products)
    }

    companion object {

        private val DEFAULT_CAPACITY = 10
        private val NOT_FOUND = -1

        fun from(cart: Cart): CartBuilder {
            return if (cart.products == null) {
                empty()
            } else CartBuilder(ArrayList(cart.products))

        }

        fun empty(): CartBuilder {
            return CartBuilder(ArrayList(DEFAULT_CAPACITY))
        }
    }
}
