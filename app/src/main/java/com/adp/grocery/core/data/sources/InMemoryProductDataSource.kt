package com.adp.grocery.core.data.sources

import com.adp.grocery.core.data.util.TimeDelayer
import com.adp.grocery.core.domain.product.Product
import com.adp.grocery.core.domain.product.ProductDataSource
import io.reactivex.Single

class InMemoryProductDataSource(private val timeDelayer: TimeDelayer) : ProductDataSource {

    override fun getAllCatalog(): Single<List<Product>> {
        return Single.fromCallable {
            timeDelayer.delay()
            ProductProvider.productList
        }
    }
}
