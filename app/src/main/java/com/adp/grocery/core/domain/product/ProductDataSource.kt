package com.adp.grocery.core.domain.product

import io.reactivex.Single

interface ProductDataSource {
    val allCatalog: Single<List<Product>>
}
