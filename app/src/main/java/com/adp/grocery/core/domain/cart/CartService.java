package com.adp.grocery.core.domain.cart;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.SingleTransformer;

public class CartService {

    private final CartDataSource cartDataSource;
    private final CartStore cartStore;

    public CartService(CartDataSource cartDataSource, CartStore cartStore) {
        this.cartDataSource = cartDataSource;
        this.cartStore = cartStore;
    }

    public Observable<Cart> getCart() {
        return cartDataSource.getCart()
                .compose(cartPublisher())
                .toObservable()
                .flatMap(cart -> cartStore.observe());
    }

    public Single<Cart> addProduct(CartProduct cartProduct) {
        return cartDataSource.addProduct(cartProduct)
                .compose(cartPublisher());
    }

    public Single<Cart> removeProduct(CartProduct cartProduct) {
        return cartDataSource.removeProduct(cartProduct)
                .compose(cartPublisher());
    }

    public Completable clear() {
        return cartDataSource.emptyCart()
                .compose(cartPublisher())
                .toCompletable();
    }

    private SingleTransformer<Cart, Cart> cartPublisher() {
        return cartObservable -> cartObservable.doOnSuccess(cartStore::publish);
    }
}
