package com.adp.grocery.core.domain.cart


class Cart(val products: List<CartProduct>?) {
    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false

        val cart = o as Cart?

        return if (products != null) products == cart!!.products else cart!!.products == null
    }

    override fun hashCode(): Int {
        return products?.hashCode() ?: 0
    }

    companion object {
        val EMPTY = Cart(emptyList())
    }
}
