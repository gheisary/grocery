package com.adp.grocery.inject


import com.adp.grocery.core.domain.cart.CartService
import com.adp.grocery.core.domain.cart.CartStore
import com.adp.grocery.core.domain.product.ProductService

interface DependenciesFactory {
    fun createCartService(): CartService

    fun createProductService(): ProductService

    fun createCartStore(): CartStore
}
