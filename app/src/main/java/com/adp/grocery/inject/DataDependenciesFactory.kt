package com.adp.grocery.inject


import com.adp.grocery.core.domain.cart.CartDataSource
import com.adp.grocery.core.domain.product.ProductDataSource


interface DataDependenciesFactory {
    fun createCartDataSource(): CartDataSource

    fun createProductDataSource(): ProductDataSource
}
