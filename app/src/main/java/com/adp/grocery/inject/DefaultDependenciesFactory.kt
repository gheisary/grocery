package com.adp.grocery.inject


import com.adp.grocery.core.domain.cart.CartService
import com.adp.grocery.core.domain.cart.CartStore
import com.adp.grocery.core.domain.product.ProductService

class DefaultDependenciesFactory(private val dataDependenciesFactory: DataDependenciesFactory) : DependenciesFactory {
    private val cartStore: CartStore

    init {
        cartStore = CartStore()
    }

    override fun createCartService(): CartService {
        return CartService(dataDependenciesFactory.createCartDataSource(), cartStore)
    }

    override fun createProductService(): ProductService {
        return ProductService(dataDependenciesFactory.createProductDataSource())
    }

    override fun createCartStore(): CartStore {
        return cartStore
    }
}
