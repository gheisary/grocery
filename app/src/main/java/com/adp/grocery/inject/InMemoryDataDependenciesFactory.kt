package com.adp.grocery.inject

import com.adp.grocery.core.data.sources.InMemoryCartDataSource
import com.adp.grocery.core.data.sources.InMemoryProductDataSource
import com.adp.grocery.core.data.util.TimeDelayer
import com.adp.grocery.core.domain.cart.CartDataSource
import com.adp.grocery.core.domain.product.ProductDataSource

class InMemoryDataDependenciesFactory : DataDependenciesFactory {

    private val timeDelayer: TimeDelayer

    init {
        timeDelayer = TimeDelayer()
    }

    override fun createCartDataSource(): CartDataSource {
        return InMemoryCartDataSource(timeDelayer)
    }

    override fun createProductDataSource(): ProductDataSource {
        return InMemoryProductDataSource(timeDelayer)
    }
}
